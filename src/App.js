import React, { Component } from "react";
import {ProductList} from "./components/ProductList";
import {Header} from "./components/Header";
import { Modal } from "./components/Modal";
import { Button } from "./components/Button";
import { ModalAgreeButtonBlock } from "./components/style/StyleComponents";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal:false,
      favoriteItems: JSON.parse(localStorage.getItem("favorites")) == null ? [] : JSON.parse(localStorage.getItem("favorites")),
      buyItems: JSON.parse(localStorage.getItem("buyList")) == null ? [] : JSON.parse(localStorage.getItem("buyList")),
      products: [],
      isModalVisible: false,
      productChoose:[],
    };
  }

  componentDidMount(){
    fetch("./data.json", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(data => this.setState({products: data}))
    .catch(error => console.error(error));
  }

  handleAddToBasket = (productId) => {
    const {products } = this.state;
    const product = products.find((p) => p.id === productId);
    if (product) {
      this.setState({ isModalVisible: true,
      productChoose: product
      });
    }
  };



  handleAddToFavorites = (productId) => {
    const { favoriteItems, products } = this.state;
    const product = products.find((p) => p.id === productId);
    if (product) {
      if (favoriteItems.includes(productId)) {
            this.setState({
        favoriteItems: favoriteItems.filter(fav => fav !== productId)
      });
    } else {
      this.setState({
        favoriteItems: [...favoriteItems, productId]})

    }

  }};

  componentDidUpdate(){
    localStorage.setItem("favorites", JSON.stringify(this.state.favoriteItems));
    localStorage.setItem("buyList", JSON.stringify(this.state.buyItems))
  }


  render() {
    const { buyItems, favoriteItems } = this.state;
    const productList = this.state.products.map(product => ({
      ...product,
      isFavorite: favoriteItems.includes(product.id)
  }))
  console.log('render');
    return (
      <div className="App">
        <Header 
          buyItems={buyItems} 
          favoriteItems={favoriteItems} 
          products={productList} 
          handleAddToBasket={this.handleAddToBasket}
          handleAddToFavorites={this.handleAddToFavorites}/>
        <ProductList
          products={productList}
          handleAddToBasket={this.handleAddToBasket}
          handleAddToFavorites={this.handleAddToFavorites}
        />


{this.state.isModalVisible && (<Modal
          className='modal-buy'
          header='Good choice!' 
          text={`Do you want to buy " ${this.state.productChoose.name} " ?`}
          onClose={() => this.setState({ isModalVisible: false })}
          actions={
            <ModalAgreeButtonBlock className="buttons">
              <Button
                backgroundColor='green'
                text='Yes!'
                handleClick={() => {
                  this.setState({
                    buyItems: [...buyItems, this.state.productChoose.id],
                    isModalVisible: false
                  });
                }}
              />
              <Button
                text='No'
                backgroundColor='red'
                handleClick={() => {
                  this.setState({ isModalVisible: false });
                }}
              />
            </ModalAgreeButtonBlock>
          }
        />)}




      </div>
    );
  }
}

export default App;






