import React from "react"
import { Button } from "./Button"
import { Wrapper,ModalAgree,ModalAgreeHeader } from "./style/StyleComponents";
import PropTypes from "prop-types";


export class Modal extends React.Component {
     render(){
        return(
            <Wrapper 
            onClick={() => {
                this.props.onClose()
            }}>
                <ModalAgree
                onClick={(e) => {
                    e.stopPropagation();
                }}>
                <ModalAgreeHeader className={`${this.props.className}__header`}>
                    <h1 className={`${this.props.className}__title`}>{this.props.header}</h1>
                    <Button className={`${this.props.className}__close`} text='X' handleClick={(e) =>{
                        this.props.onClose();
                    }}/>
                </ModalAgreeHeader>
                <p className={`${this.props.className}__text`}>{this.props.text}</p>
                {this.props.actions}
                </ModalAgree>
            </Wrapper>
        )
     }
}


Modal.propTypes = {
    className:PropTypes.string,
    header:PropTypes.string,
    onClose: PropTypes.func,
}