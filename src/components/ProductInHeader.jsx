import React from "react";
import { Button } from "./Button"
import { } from "./style/StyleComponents";
import PropTypes from "prop-types";

export class ProductInHeader extends React.Component {

  render() {
    const { product } = this.props;
    return (
      <div className="product">
        <h3>{product.name}</h3>
        <img src={product.image} alt="" />
        <p>Price: {product.price} грн</p>
        <p>Article: {product.article}</p>
        <p style={{
          backgroundColor:product.color,
          borderRadius: '50%',
          width: '20px',
          height: '20px',
          }}></p>

          <Button 
            text='Buy' 
            className='button-buy'
            handleClick={() => {this.props.handleAddToBasket(product.id)}}
            backgroundColor='#3A93FF'
          />
          <svg         
            className='button-favorite'
            onClick={() =>{
            this.props.handleAddToFavorites(product.id);
            } }
            viewBox="0 0 30 30"
            width="40"
            height="40"
            fill={product.isFavorite ? "green" : "none"}
            stroke="currentColor"
            strokeWidth="1"
            strokeLinecap="round"
            strokeLinejoin="round">
            <path d="M12 2 L15.09 8.45 L22 9.55 L17 14.24 L18.18 21.01 L12 17.77 L5.82 21.01 L7 14.24 L2 9.55 L8.91 8.45 Z" />
          </svg>
      </div>
    );
  }
}



ProductInHeader.propTypes = {
    product: PropTypes.object,
    onModalShowHandler: PropTypes.func,
    addFavorites: PropTypes.func,
    deleteFavorites: PropTypes.func,
}