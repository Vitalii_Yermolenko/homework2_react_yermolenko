import React from "react";
import { ProductInHeader } from "./ProductInHeader";
import PropTypes from "prop-types";

export class ListInHeader extends React.Component {

    
    
    render() {
        return (
          <div
            className='wrapper'
            onClick={() => {
              this.props.onClose();
            }}
          >
            <div
              className={this.props.className}
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              <h1>{this.props.header}</h1>
              <ol>
                {this.props.products
                  .filter((product) => 
                  this.props.productsId.includes(product.id))
                  .map((product) => (
                    <ProductInHeader
                      key={product.id}
                      product={product}
                      handleAddToBasket={this.props.handleAddToBasket}
                      handleAddToFavorites={this.props.handleAddToFavorites}
                    />
                  ))}
              </ol>
            </div>
          </div>
        );
      }

}


ListInHeader.propTypes = {
  productsId: PropTypes.object,
  products: PropTypes.object,
  handleAddToBasket: PropTypes.func,
  handleAddToFavorites: PropTypes.func,
  onClose: PropTypes.func,
}