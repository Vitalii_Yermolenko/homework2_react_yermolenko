import React from "react";
import PropTypes from "prop-types";

export class Button extends React.Component{
    render(){
        return(
            <button type="button"
            className={this.props.className}
            style={{backgroundColor: this.props.backgroundColor}}
            onClick={(e) => {
                this.props.handleClick()
            }}
            >{this.props.text}</button>
        )

    }
}

Button.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    handleClick: PropTypes.func,
}