import React from "react";
import { Product } from "./Product";
import { CardsWrapper } from "./style/StyleComponents";
import PropTypes from "prop-types";



export class ProductList extends React.Component {
  render() {
    return (
      <CardsWrapper className="products">
        {this.props.products.map((product) => (
          <Product
            key={product.id}
            product={product}
            handleAddToBasket={this.props.handleAddToBasket}
            handleAddToFavorites={this.props.handleAddToFavorites}
          />
        ))}
      </CardsWrapper>
    );
  }
}


ProductList.propTypes = {
  products: PropTypes.array,
  handleAddToBasket: PropTypes.func,
  handleAddToFavorites: PropTypes.func,
}